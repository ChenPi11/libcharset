/* Define it if you have the <langinfo.h> header file, undef it if not. */
#cmakedefine HAVE_LANGINFO_H

#ifdef HAVE_LANGINFO_H
#define HAVE_LANGINFO_CODESET 1
#else
#define HAVE_LANGINFO_CODESET 0
#endif