# Libcharset

**This library is based on GNU Libcharset but not!**

## Introduce
This library provides a function which determines the character set / encoding
of text in the currently selected locale (the LC_CTYPE locale facet).

It is useful for portable programs which need to process text in other
encodings and locales than the currently selected one. Possible uses:

  * Use of Unicode in POSIX compliant applications.
  * Conversion of text between the current locale's encoding and UTF-8 (or
    any other given encoding).
  * Mail agents.

In theory, this would be very simple: POSIX provides the nl_langinfo function,
in such a way that
```c
nl_langinfo (CODESET)
```
returns the encoding name. But the nl_langinfo function still does not exist
on some systems, and on those where it exists it returns unstandardized
variations of the encoding names, like (on Solaris) "PCK" for "Shift_JIS".

This library fixes these flaws and provides a function
```c
const char * locale_charset (void);
```
It determines the current locale's character encoding, and canonicalizes it
into one of the canonical names listed in localcharset.h. The result must
not be freed; it is statically allocated. If the canonical name cannot be
determined, the result is a non-canonical name.

## Installation/Build
### Use GNU Make
As usual for GNU packages:
```shell
./configure --prefix=/usr/local
make
make install
```

If you don't want the configure result to contaminate the repository, you can use this:
```shell
mkdir build
cd build
../configure --prefix=/usr/local
make
make install
```

If you want to uninstall, you can:
```shell
make uninstall
```

If you want to clean, you can:
```shell
make clean
```

### Use CMake
```shell
mkdir build
cd build
cmake ..
# Then, you should run 'make' or 'msbuild charset.sln' to build.
# run 'make install' on Unix/Linux/Darwin... to install
```

## This library is used in
+ cppp.libiconv

## GNU Libcharset infomation
### GNU Libcharset Distribution:
The libcharset directory of ftp://ftp.gnu.org/pub/gnu/libiconv/libiconv-1.14.tar.gz

### GNU Libcharset Homepage:
https://haible.de/bruno/packages-libcharset.html

## Authors
+ Bruno Haible <bruno@clisp.org>
+ ChenPi12 <yuesefusidalin@outlook.com>